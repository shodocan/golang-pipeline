package entities

type Student struct {
	Id        string
	Name      string
	Age       int
	MathGrade int
}
