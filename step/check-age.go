package step

import (
	"log"
	"math/rand"

	"gitlab.com/shodocan/golang-pipeline/entities"
)

type CheckAge struct {
}

func (c *CheckAge) Run(input <-chan interface{}) <-chan interface{} {
	output := make(chan interface{})
	rand.Seed(2018)
	go func() {
		for message := range input {
			student, ok := message.(entities.Student)
			if ok {
				student.Age = 10 + rand.Intn(10)
				output <- student
			} else {
				log.Println("couldnt convert %v to student", message)
			}
		}
		close(output)
	}()
	return output
}
