package step

import (
	randomdata "github.com/Pallinder/go-randomdata"
	"gitlab.com/shodocan/golang-pipeline/entities"
)

type GenerateStudent struct {
	Ammount int
}

func (g *GenerateStudent) Run() <-chan interface{} {
	output := make(chan interface{})
	go func() {
		for i := 0; i < g.Ammount; i++ {
			output <- entities.Student{Name: g.getRandonName()}
		}
		close(output)
	}()
	return output
}

func (g *GenerateStudent) getRandonName() string {
	return randomdata.FullName(randomdata.RandomGender)
}
