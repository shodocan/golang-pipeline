package step

import (
	"log"

	"gitlab.com/shodocan/golang-pipeline/entities"
)

type RankStudents struct {
}

func (step *RankStudents) Run(input <-chan interface{}) <-chan interface{} {
	output := make(chan interface{})
	rank := make(map[int][]entities.Student)
	go func() {
		for message := range input {
			student, ok := message.(entities.Student)
			if ok {
				if currentRank, ok := rank[student.Age]; ok {
					if currentRank[0].MathGrade < student.MathGrade {
						rank[student.Age] = []entities.Student{student}
						output <- rank
					} else if currentRank[0].MathGrade == student.MathGrade {
						rank[student.Age] = append(rank[student.Age], student)
						output <- rank
					}
				} else {
					rank[student.Age] = []entities.Student{student}
					output <- rank
				}
			} else {
				log.Println("couldnt convert %v to student", message)
			}
		}
		close(output)
	}()
	return output
}
