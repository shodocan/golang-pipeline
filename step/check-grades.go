package step

import (
	"log"
	"math/rand"
	"time"

	"gitlab.com/shodocan/golang-pipeline/entities"
)

type CheckGrades struct {
}

func (c *CheckGrades) Run(input <-chan interface{}) <-chan interface{} {
	output := make(chan interface{})
	rand.Seed(2018)
	go func() {
		for message := range input {
			time.Sleep(100 * time.Millisecond)
			student, ok := message.(entities.Student)
			if ok {
				for i := 0; i < 10; i++ {
					student.MathGrade += rand.Intn(10)
				}
				output <- student
			} else {
				log.Println("couldnt convert %v to student", message)
			}
		}
		close(output)
	}()
	return output
}
