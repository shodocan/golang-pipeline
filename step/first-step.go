package step

type FirstStep interface {
	Run() <-chan interface{}
}
