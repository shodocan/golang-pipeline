package step

import "sync"

type Step interface {
	Run(<-chan interface{}) <-chan interface{}
}

type DefaultStep struct {
}

type StepRunFunc func(<-chan interface{}) <-chan interface{}

func (step *DefaultStep) runParallel(input <-chan interface{}, dofn StepRunFunc, n int) <-chan interface{} {
	outputs := make([]<-chan interface{}, 0)
	for i := 0; i < n; i++ {
		output := dofn(input)
		outputs = append(outputs, output)
	}
	return step.mergeChannels(outputs...)
}

func (step *DefaultStep) mergeChannels(channels ...<-chan interface{}) <-chan interface{} {
	output := make(chan interface{})
	go func() {
		var wg sync.WaitGroup
		wg.Add(len(channels))
		for _, channel := range channels {
			go func(c <-chan interface{}) {
				for message := range c {
					output <- message
				}
				wg.Done()
			}(channel)
		}
		wg.Wait()
		close(output)
	}()
	return output
}
