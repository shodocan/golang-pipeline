package main

import (
	"fmt"
	"log"
	"sort"
	"time"

	"gitlab.com/shodocan/golang-pipeline/step"

	"gitlab.com/shodocan/golang-pipeline/entities"
	"gitlab.com/shodocan/golang-pipeline/pipeline"
)

func main() {
	startTime := time.Now()
	doPipeline()
	log.Println(time.Since(startTime))
}

func doPipeline() {
	pipeline1 := &pipeline.DefaultPipeline{FirstStep: &step.GenerateStudent{Ammount: 1000}}
	output := pipeline1.Step(&step.CheckAge{}).Step(&step.CheckGrades{}).Step(&step.RankStudents{}).Run()
	for o := range output {
		rank, ok := o.(map[int][]entities.Student)
		if ok {
			log.Println(formatRank(rank))
		}
		log.Println("----")
	}
}

func formatRank(rank map[int][]entities.Student) (output string) {
	var ages []int
	for age := range rank {
		ages = append(ages, age)
	}
	sort.Ints(ages)
	for _, age := range ages {
		output = output + fmt.Sprintf("age: %d \n", age)
		for _, student := range rank[age] {
			output = output + fmt.Sprintf("Name: %s, Grade: %d \n", student.Name, student.MathGrade)
		}
	}
	output = output + "\n"
	return
}
