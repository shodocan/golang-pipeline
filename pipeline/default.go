package pipeline

import (
	"gitlab.com/shodocan/golang-pipeline/step"
)

type DefaultPipeline struct {
	FirstStep step.FirstStep
	steps     []step.Step
}

func (p *DefaultPipeline) Step(step step.Step) *DefaultPipeline {
	p.steps = append(p.steps, step)
	return p
}

func (pipeline *DefaultPipeline) Run() <-chan interface{} {
	output := pipeline.FirstStep.Run()
	for _, step := range pipeline.steps {
		output = step.Run(output)
	}
	return output
}
